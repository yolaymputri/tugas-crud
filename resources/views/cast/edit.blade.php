@extends('layout.master')

@section('judul')
    Halaman List Cast
@endsection

@section('isi')

<div>
    <form action="/cast/{{$cast->id}}" method="POST">
        @method('put')
        @csrf
        <div class="form-group">
            <label>nama</label>
            <input type="text" class="form-control" name="nama" value={{$cast->nama}} placeholder="Masukkan Nama Cast">
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>umur</label>
            <input type="text" class="form-control" name="umur" value={{$cast->umur}}  placeholder="Masukkan Umur">
            @error('umur')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label>bio</label>
            <input type="textarea" class="form-control" name="bio" value={{$cast->bio}} placeholder="Masukkan Bio">
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
            
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
    </form>
</div>
@endsection